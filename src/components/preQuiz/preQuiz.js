import React, { useState, useEffect, useContext, Fragment } from 'react';
import config from 'visual-config-exposer';
import Styled from 'styled-components';

import { GameContext } from '../../context/gameContext';
import LeaderBoard from '../LeaderBoard/LeaderBoard';
import SoundOn from '../../../public/assets/volume-2.svg';
import SoundOff from '../../../public/assets/volume-x.svg';
import './preQuiz.css';

const logo = config.preQuizScreen.logo;
const title = config.preQuizScreen.title;
const playBtnText = config.preQuizScreen.playBtnText;
const ldbBtnText = config.preQuizScreen.leaderBoardBtnText;

const PlayButton = Styled.button`
background-color: ${config.preQuizScreen.playBtnColor};
color: ${config.preQuizScreen.playBtnTextColor};
`;

const LeaderBoardButton = Styled.button`
background-color: ${config.preQuizScreen.leaderBoardBtnColor};
color: ${config.preQuizScreen.leaderBoardBtnTextColor};
`;

const PreQuiz = (props) => {
  const gameContext = useContext(GameContext);
  let image = props.soundOn ? SoundOn : SoundOff;
  const [showLeaderBoard, setShowLeaderBoard] = useState(false);

  const startGameHandler = () => {
    gameContext.setMainScreen();
  };

  const changeLeaderBoardHandler = () => {
    setShowLeaderBoard(!showLeaderBoard);
  };

  const cardJsx = showLeaderBoard ? (
    <article className="pre__card">
      <LeaderBoard />
      <LeaderBoardButton className="button" onClick={changeLeaderBoardHandler}>
        back
      </LeaderBoardButton>
    </article>
  ) : (
    <article className="pre__card">
      <div className="sound__image" onClick={props.soundHandler}>
        <img src={image} alt="sound" />
      </div>
      <div className="pre__logo">
        <img src={logo} alt="logo" className="pre__img" />
      </div>
      <h1 className="pre__title">{title}</h1>
      <div className="pre__btn-container">
        <PlayButton onClick={startGameHandler} className="button">
          {playBtnText}
        </PlayButton>
        <LeaderBoardButton
          onClick={changeLeaderBoardHandler}
          className="button"
        >
          {ldbBtnText}
        </LeaderBoardButton>
      </div>
    </article>
  );

  return <section>{cardJsx}</section>;
};

export default PreQuiz;
